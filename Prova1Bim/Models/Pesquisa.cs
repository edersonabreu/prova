﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Prova1Bim.Models
{
    public class Pesquisa
    {
        private string _nome;

        [DisplayName("Nome")]
        [Required(ErrorMessage = "Insira o nome para pesquisa ! ")]
        public string Nome
        {
            get
            {
                return _nome;
            }

            set
            {
                _nome = value;
            }
        }
    }
}