﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Prova1Bim.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Cadastro()
        {
            return View();
        }
        public ActionResult Encaminhar(Models.Cliente cli)
        {
            if(!string.IsNullOrEmpty("Gravar"))
            {
                if (cli.Gravar(cli))
                {
                    ViewBag.Msg = "Gravado com sucesso !!";
                }
                return View("Cadastro");
            }
            else
            {
                return View("Cancelar");
            }
        }
        public ActionResult Gravar(Models.Cliente cli)
        {
            if (ModelState.IsValid)
            {
                if (cli.Gravar(cli))
                {
                    ViewBag.Msg = "Gravado com sucesso !!";
                }
                return View("Cadastro");
            }
            else
            {
                ViewBag.Msg = "Erro ao gravar";
                return View("Cadastro");
            }
        }
        public ActionResult Cancelar(Models.Cliente cli)
        {
            return View("Cadastro");
        }
    }
}