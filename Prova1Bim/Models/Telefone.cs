﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Prova1Bim.Models
{
    public class Telefone
    {
        private int _id;
        private int _numero;
        private string _tipo;
        private int _idContato;

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }
        [DisplayName("Numero")]
        [Required(ErrorMessage = "Digite o numero ! ")]
        public int Numero
        {
            get
            {
                return _numero;
            }

            set
            {
                _numero = value;
            }
        }

        [DisplayName("Tipo")]
        [Required(ErrorMessage = "Insira o tipo ! ")]
        public string Tipo
        {
            get
            {
                return _tipo;
            }

            set
            {
                _tipo = value;
            }
        }

        [DisplayName("Id")]
        [Required(ErrorMessage = "Insira o Id ! ")]
        public int IdContato
        {
            get
            {
                return _idContato;
            }

            set
            {
                _idContato = value;
            }
        } 
    }
}