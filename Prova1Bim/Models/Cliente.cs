﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Prova1Bim.Models
{
    public class Cliente
    {
        private int _id; 
        private string _nome;
        private DateTime _data;
        private string _email;
        Persistencia _bd = new Persistencia();

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        [DisplayName("Nome")]
        [Required(ErrorMessage = "Digite seu nome ! ")]
        public string Nome
        {
            get
            {
                return _nome;
            }

            set
            {
                _nome = value;
            }
        }

        [DisplayName("Data")]
        [Required(ErrorMessage = "Digite a data ! ")]
        public DateTime Data
        {
            get
            {
                return _data;
            }

            set
            {
                _data = value;
            }
        }

        [DisplayName("Email")]
        [Required(ErrorMessage = "Digite seu email ! ")]
        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
            }
        }
        public bool Gravar(Models.Cliente cliente)
        {
            string sql = "";
            try
            {
                if(Id == 0)
                {
                    sql = "INSERT INTO prova1bim.contato (nome, email, datanasc) values (@pnome, @pemail, @pdatanasc)";
                }
                else
                {
                    sql = "UPDATE prova1bim.contato (nome = @pnome, email = @pemail, datanasc = @pdatanasc) WHERE id =" + Id;
                }
                List<MySqlParameter> parametros = new List<MySqlParameter>();
                parametros.Add(new MySqlParameter("@pnome", Nome));
                parametros.Add(new MySqlParameter("@pemail", Email));
                parametros.Add(new MySqlParameter("@pdatanasc", Convert.ToDateTime(Data).Date));
                if(Id == 0)
                {
                    int id = (int)_bd.ExecuteNonQueryLastKey(sql, parametros);
                }
                else
                {
                    _bd.ExecuteNonQuery(sql, parametros);
                }
                return true;
            }
            catch
            {
                return false;
            }
           
        }
    }
}